#!/usr/bin/env bash

set -e

function dumpLogsAndExit{
    docker-compose build logs
    exit $1
}
echo "Build images for Selenoid and Selenoid UI"
docker-compose build

dumpLogsAndExit 0
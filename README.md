Selenoid Deployment Commands:

Browsers.json:

- Edit browsers.json file by adding needed browsers

Deploying selenoid:

- docker-compose -p selenoid up -d --build

Pull Browsers Images Defined in browsers.json file: 

- docker-compose -p selenoid exec selenoid ./pull-browsers.sh

Stoping Selenoid Deployment: 

- docker-compose -p selenoid down --remove-orphans

WARNING: docker-compose command must use from the directory in which it is located

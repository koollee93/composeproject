FROM aerokube/selenoid:1.10.5
WORKDIR /
RUN apk update && \
    apk add jq && \
    apk add --update docker openrc && \
    rc-update add docker boot

COPY browsers.json /etc/selenoid/
COPY pull-browsers.sh /
RUN chmod +x pull-browsers.sh
EXPOSE 4444
ENTRYPOINT ["/usr/bin/selenoid", "-listen", ":4444", "-conf", "/etc/selenoid/browsers.json", "-video-output-dir", "/opt/selenoid/video/"]

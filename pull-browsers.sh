#!/usr/bin/env ash
cat /etc/selenoid/browsers.json | jq -r '..|.image?|strings' | xargs -I{} docker pull {}
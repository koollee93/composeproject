#!/usr/bin/env bash

set -e

function dumpLogsAndExit{
    docker-compose -p selenoid logs
    exit $1
}
echo "Build images for Selenoid and Selenoid UI"
docker-compose -p selenoid up --remove-orphans

dumpLogsAndExit 0